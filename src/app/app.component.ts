import {Component, OnInit} from '@angular/core';
import {Zoo} from "./classes/zoo";
import {Animal} from "./classes/animal";
import {Penguin} from "./classes/penguin";
import {Guest} from "./classes/guest";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public zoo: Zoo;

  constructor() {
    this.zoo = new Zoo();
  }

  ngOnInit(): void {
    // this.zoo.addAnimal(new Animal('Does not work'));
    this.zoo.addAnimal(new Penguin('Pingu'));
    this.zoo.addAnimal(new Penguin('Barry'));

    this.zoo.addGuest(new Guest('Sam'));
  }
}
