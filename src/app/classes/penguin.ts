import {Animal} from "./animal";

export class Penguin extends Animal {
  speak(): void {
    console.log('Cheep, cheep, cheep, cheep!');
  }
}
