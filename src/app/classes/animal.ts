export abstract class Animal {
  public constructor(protected name: string) { }

  getName(): string {
    return this.name;
  }

  abstract speak(): void;
}
