import {Animal} from "./animal";
import {Guest} from "./guest";

export class Zoo {
  private animals: Array<Animal>;
  private guests: Array<Guest>;

  constructor() {
    this.animals = new Array<Animal>();
    this.guests = new Array<Guest>();
  }

  public getAnimals(): Array<Animal> {
    return this.animals;
  }

  public getGuests(): Array<Guest> {
    return this.guests;
  }

  public addAnimal(animal: Animal) {
    this.animals.push(animal);
  }

  public addGuest(guest: Guest) {
    this.guests.push(guest);
  }
}
