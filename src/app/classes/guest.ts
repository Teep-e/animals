export class Guest {
  constructor(protected name: string) {}

  getName(): string {
    return this.name;
  }

  speak() {
    console.log(`Hello!  My name is ${this.name}`);
  }
}
